package patientview;
import java.awt.Color;
import java.awt.*;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.IOException;
import java.text.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.util.Date;
import java.awt.Dimension;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author s1513746
 */
public class user2 extends javax.swing.JFrame {
private Timer timer;
    private int timerSeconds;
    public static final int RESPIRATORY_RATE = 0;
    public static final int SPO2 = 1;
    public static final int SYSTOLIC = 2;
    public static final int HEART_RATE = 3;
    public static final float TEMPERATURE = 0f;
    int count=0;
    int count_res=0;
    int count_oxy=0;
    int count_blo=0;
    int count_hr =0;
    int count_tem=0;
    /**
     * Creates new form user2
     */
    public user2() {
        initComponents();
        this.getContentPane().setBackground(Color.WHITE);
        timerSeconds=0; 
      if (timer == null){
      timer = new Timer(1000, new ActionListener() {
      public void actionPerformed(ActionEvent e) {       
      Calendar cal = Calendar.getInstance(); 
      cal.getTime(); 
      SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss"); 
      date_txt.setText(sdf.format(cal.getTime()));
      int br = 0; int spo2 = 0; int systolic = 0; int hr = 0; float temp=0f;
      if (timerSeconds % 5 == 0){
      br = generateReading(RESPIRATORY_RATE);
      spo2 = generateReading(SPO2);
      temp= generateReading(TEMPERATURE);
      systolic= generateReading(SYSTOLIC);
      hr= generateReading(HEART_RATE);
      rate.setText(Integer.toString(br));
      oxygen.setText(Integer.toString(spo2));
      blood_pressure.setText(Integer.toString(systolic));
      heart_rate.setText(Integer.toString(hr));
      String str= String.valueOf(temp);
      temperature.setText(str);
      breath_table(br);
      oxygen_table(spo2);
      temperature_table(temp);
      blood_table(systolic);
      heartrate_table(hr);
      count=count_res+count_oxy+count_blo+count_hr+count_tem;
      traffic_light(count-6);
               }
               timerSeconds++;
      count_res=0;
      count_oxy=0;
      count_blo=0;
      count_hr =0;
      count_tem=0;
      count=0;
             }
          });
          
       
       }
      if (timer.isRunning() == false) {
            timer.start();
        }
  }
   public void breath_table(int br){
       int value;
       if (br<=8)
           value=3;
       else if (br>=9 && br<=20)
           value=0;
       else if (br>=21 && br<=30)
           value=1;
       else if (br>=31 && br<=35)
           value=2;
       else
           value=3;
       count_res=count_res+value;
   }
   
      public void oxygen_table(int spo2){
       int value;
       if (spo2<85)
           value=3;
       else if (spo2>=85 && spo2<=89)
           value=2;
       else if (spo2>=90 && spo2<=92)
           value=1;
       else
           value=0;
       count_oxy=count_oxy+value;
   }
      
      
      
      
      public void temperature_table(float temp){
       int value;
       if (temp<34.0)
           value=3;
       else if (temp>=34.0 && temp<=34.9)
           value=2;
       else if (temp>=35.0 && temp<=35.9)
           value=1;
       else if (temp>=36.0 && temp<=37.9)
           value=0;
       else if (temp>=38.0 && temp<=38.4)
           value=1;
       else 
           value=2;
       count_tem=count_tem+value;
   }    
      
       public void blood_table(int systolic){
       int value;
       if (systolic<=69)
           value=3;
       else if (systolic>=70 && systolic<=79)
           value=2;
       else if (systolic>=80 && systolic<=99)
           value=1;
       else if (systolic>=100 && systolic<=199)
           value=0;
       else
           value=2;
       count_blo=count_blo+value;
   }
      
       public void heartrate_table(int hr){
       int value;
       if (hr<=29)
           value=3;
       else if (hr>=30 && hr<=39)
           value=2;
       else if (hr>=40 && hr<=49)
           value=1;
       else if (hr>=50 && hr<=99)
           value=0;
       else if (hr>=100 && hr<=109)
           value=1;
       else if (hr>=110 && hr<=129)
           value=2;
       else
           value=3;
       count_hr=count_hr+value;
   }
       
      public void traffic_light(int count){
          
       if (count==0 || count==1){
           psews.setText(Integer.toString(count));
           psews.setForeground(Color.GREEN);
       }
       else if (count==2 || count==3){
           psews.setText(Integer.toString(count));
           psews.setForeground(Color.YELLOW); 
       }
       else if (count==4 || count>=5){
           psews.setText(Integer.toString(count));
           psews.setForeground(Color.RED); 
       }   
         
      }
   public int generateReading(int kind) {
        Random random = new Random();
        int returnReading = 0;
        switch (kind) {
            case 0: //Breathing Rate
                returnReading = random.nextInt(35) + 5 + 1;
                break;
            case 1: //SPO2
                returnReading = random.nextInt(20) + 80 + 1;
                break;
            case 2: //Blood Pressure - SYSTOLIC
                returnReading = random.nextInt(150) + 60 + 1;
                break;
            case 3: //Heart Rate
                returnReading = random.nextInt(20) + 140 + 1;
                break;
            default:
                break;
        }
        
        return returnReading;
    }
    
 public float generateReading(float kind) {
        Random random = new Random();
        return random.nextFloat() * 6 + 33;
    }
    
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        blood_pressure = new javax.swing.JLabel();
        heart_rate = new javax.swing.JLabel();
        temperature = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        psews = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        date_txt = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        oxygen = new javax.swing.JLabel();
        rate = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        blood_pressure.setFont(new java.awt.Font("Cantarell", 1, 36)); // NOI18N
        blood_pressure.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        blood_pressure.setText("jLabel7");
        blood_pressure.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        heart_rate.setFont(new java.awt.Font("Cantarell", 1, 36)); // NOI18N
        heart_rate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        heart_rate.setText("jLabel8");
        heart_rate.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        temperature.setFont(new java.awt.Font("Cantarell", 1, 36)); // NOI18N
        temperature.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        temperature.setText("jLabel4");
        temperature.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel1.setText("Charlie Dean");

        psews.setFont(new java.awt.Font("Cantarell", 1, 48)); // NOI18N
        psews.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        psews.setText("jLabel4");
        psews.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jLabel2.setText("Male");

        jLabel3.setText("1962-03-02");

        date_txt.setText("Time");

        oxygen.setFont(new java.awt.Font("Cantarell", 1, 36)); // NOI18N
        oxygen.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        oxygen.setText("jLabel5");
        oxygen.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        rate.setBackground(java.awt.Color.lightGray);
        rate.setFont(new java.awt.Font("Cantarell", 1, 36)); // NOI18N
        rate.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        rate.setText("jLabel6");
        rate.setBorder(javax.swing.BorderFactory.createEtchedBorder());

        jButton1.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/home.png")); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("Oxygen saturation(%)  ");

        jLabel4.setText("Respiratory rate (breaths/min)\t   ");

        jLabel8.setText("Temperature (°C)\t   ");

        jLabel9.setText("psews value");

        jLabel7.setText("Heart rate (beats/min)\t   ");

        jLabel6.setText("Systolic blood pressure (mmHg)\t   ");

        jButton2.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/LINE.png")); // NOI18N
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jSeparator1))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel2))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(11, 11, 11)
                                        .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 168, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(101, 101, 101)
                                        .addComponent(jLabel4))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(blood_pressure, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(oxygen, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE))
                                            .addComponent(jLabel6))
                                        .addGap(81, 81, 81)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGap(12, 12, 12)
                                                .addComponent(jLabel7))
                                            .addComponent(rate, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(heart_rate, javax.swing.GroupLayout.PREFERRED_SIZE, 159, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 130, Short.MAX_VALUE)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                                    .addComponent(temperature, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel8))
                                                .addGap(107, 107, 107))
                                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                                .addGap(24, 24, 24)
                                                .addComponent(jButton2)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(layout.createSequentialGroup()
                                                .addComponent(psews, javax.swing.GroupLayout.PREFERRED_SIZE, 229, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(86, 86, 86))
                                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                    .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 93, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGap(76, 76, 76))))
                                    .addComponent(date_txt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(date_txt))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(oxygen, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(rate, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(blood_pressure, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(heart_rate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(112, 112, 112))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(56, 56, 56)
                                .addComponent(psews, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(82, 82, 82)
                                .addComponent(temperature, javax.swing.GroupLayout.PREFERRED_SIZE, 139, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton1)
                                .addGap(20, 20, 20))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(72, 72, 72))))))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        try{
            PatientJFrame j = new PatientJFrame();
            dispose();
            j.setVisible(true);
        }
        catch (IOException b){
            System.out.println("error");
        }
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
          dispose();
     HistoryJFrame s= new HistoryJFrame();
     s.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(user2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(user2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(user2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(user2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new user2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel blood_pressure;
    private javax.swing.JLabel date_txt;
    private javax.swing.JLabel heart_rate;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel oxygen;
    private javax.swing.JLabel psews;
    private javax.swing.JLabel rate;
    private javax.swing.JLabel temperature;
    // End of variables declaration//GEN-END:variables
}
