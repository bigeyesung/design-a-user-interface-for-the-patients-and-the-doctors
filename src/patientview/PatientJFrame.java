/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package patientview;
//package historyview;
import java.awt.*;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import patientview.user;
import patientview.user2;
import patientview.user3;
import patientview.user4;
import patientview.user5;
import patientview.user6;

/**
 *
 * @author Qi Zhou
 */
public class PatientJFrame extends javax.swing.JFrame {

    private Timer timer;
    private int timerSeconds;
    public static final int RESPIRATORY_RATE = 0;
    public static final int SPO2 = 1;
    public static final int SYSTOLIC = 2;
    public static final int HEART_RATE = 3;
    public static final float TEMPERATURE = 0f;
    int count=0;
    int count_res=0;
    int count_oxy=0;
    int count_blo=0;
    int count_hr=0 ;
    int count_tem=0;
    int p1;
    int p2;
    int p3;
    int p4;
    int p5;
    int p6;
    /**
     * Creates new form PatientJFrame
     */
    public PatientJFrame() throws IOException {
     
        
        this.getContentPane().setBackground(Color.WHITE); 
        initComponents();
        //set window in the center of the screen
        //Get the size of the screen
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        //Determine the new location of the window
        int w = this.getSize().width;
        int h = this.getSize().height;
        int x = (dim.width - w) / 2;
        int y = (dim.height - h) / 2;
        //Move the window
        this.setLocation(x, y);
        //fill titles
        /*patientNameField.setText("Alice Bailey");
        wardNumField.setText("W001");
        bedNumField.setText("1001");
        dobField.setText("1958-10-12");
        genderField.setText("F");*/
        /*Label hello = new Label("fuck");
        setLayout(new FlowLayout());
        add(hello);*/
        //set focus on exit button
        //jButton_exit.requestFocus();
        //set timer
        timerSeconds = 0;
        if (timer == null) {
            timer = new Timer(1000, new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // do it every 1 second
                    Calendar cal = Calendar.getInstance();
                    cal.getTime();
                    SimpleDateFormat sdf = new SimpleDateFormat("YYYY-MM-dd HH:mm:ss");
                    Time.setText(sdf.format(cal.getTime()));
                    // do it every 5 seconds
                    if (timerSeconds % 5 == 0) {
                        //generate random value
                        int br = generateReading(RESPIRATORY_RATE);
                        int spo2 = generateReading(SPO2);
                        float temp = generateReading(TEMPERATURE);
                        int systolic = generateReading(SYSTOLIC);
                        int hr = generateReading(HEART_RATE);
                        breath_table(br);
                        oxygen_table(spo2);
                        temperature_table(temp);
                        blood_table(systolic);
                        heartrate_table(hr);
                        count=count_res+count_oxy+count_blo+count_hr+count_tem;
                        p1= count_random(count);
                        p2 = count_random(count);
                        p3 = count_random(count);
                        p4 = count_random(count);
                        p5 = count_random(count);
                        p6 = count_random(count);
                        //Alice.setText(Integer.toString(p1));
                        Alice_light(p1);
                        Charlie_light(p2);
                        Grace_light(p3);
                        Ian_light(p4);
                        Elise_light(p5);
                        Kelly_light(p6);
                
                    }
                    timerSeconds++;
                      count_res=0;
                      count_oxy=0;
                      count_blo=0;
                      count_hr =0;
                      count_tem=0;
                      count=0;     
                }
            });
        }

        if (timer.isRunning() == false) {
            timer.start();
        }
    }

  public void Alice_light(int i){
          
       if (i==0 || i==1){
           Alice.setText(Integer.toString(p1));
           Alice.setForeground(Color.GREEN);
       }
       else if (i==2 || i==3){
           Alice.setText(Integer.toString(p1));
           Alice.setForeground(Color.YELLOW); 
       }
       else if (i==4 || i>=5){
           Alice.setText(Integer.toString(p1));
           Alice.setForeground(Color.RED); 
       }   
         
      }   
  
   public void Charlie_light(int i){
          
       if (i==0 || i==1){
           Charlie.setText(Integer.toString(p2));
           Charlie.setForeground(Color.GREEN);
       }
       else if (i==2 || i==3){
           Charlie.setText(Integer.toString(p2));
           Charlie.setForeground(Color.YELLOW); 
       }
       else if (i==4 || i>=5){
           Charlie.setText(Integer.toString(p2));
           Charlie.setForeground(Color.RED); 
       }   
         
      }  
  
 public void Grace_light(int i){
          
       if (i==0 || i==1){
           Grace.setText(Integer.toString(p3));
           Grace.setForeground(Color.GREEN);
       }
       else if (i==2 || i==3){
           Grace.setText(Integer.toString(p3));
           Grace.setForeground(Color.YELLOW); 
       }
       else if (i==4 || i>=5){
           Grace.setText(Integer.toString(p3));
           Grace.setForeground(Color.RED); 
       }   
         
      }   
   
 
  public void Ian_light(int i){
          
       if (i==0 || i==1){
           Ian.setText(Integer.toString(p4));
           Ian.setForeground(Color.GREEN);
       }
       else if (i==2 || i==3){
           Ian.setText(Integer.toString(p4));
           Ian.setForeground(Color.YELLOW); 
       }
       else if (i==4 || i>=5){
           Ian.setText(Integer.toString(p4));
           Ian.setForeground(Color.RED); 
       }   
         
      }   
 
    public void Elise_light(int i){
          
       if (i==0 || i==1){
           Elise.setText(Integer.toString(p6));
           Elise.setForeground(Color.GREEN);
       }
       else if (i==2 || i==3){
           Elise.setText(Integer.toString(p6));
           Elise.setForeground(Color.YELLOW); 
       }
       else if (i==4 || i>=5){
           Elise.setText(Integer.toString(p6));
           Elise.setForeground(Color.RED); 
       }   
         
      }  
   
      public void Kelly_light(int i){
          
       if (i==0 || i==1){
           Kelly.setText(Integer.toString(p5));
           Kelly.setForeground(Color.GREEN);
       }
       else if (i==2 || i==3){
           Kelly.setText(Integer.toString(p5));
           Kelly.setForeground(Color.YELLOW); 
       }
       else if (i==4 || i>=5){
           Kelly.setText(Integer.toString(p5));
           Kelly.setForeground(Color.RED); 
       }   
         
      } 
    
 public void Alice_judge(int i){
 String[] medStrings = { "Continue routine observation", "Involve the nurse-in-charge immediately", "Call the registrar for immediate review" };
//Create the combo box, select item at index 4.
//Indices start at 0, so 4 specifies the pig.
JComboBox medList = new JComboBox(medStrings);
    int get = Alice_selection.getSelectedIndex();
if(get+i==0|| get+i==1)
    Alice_output.setText(medStrings[0]);
else if(get+i==2 || get+i==3)
    Alice_output.setText(medStrings[0]);
else if(get+i==4 || get+i==5)
    Alice_output.setText(medStrings[1]);
else if(get+i>=6)
    Alice_output.setText(medStrings[2]);

          
    
     
 }    
    
public void Charlie_judge(int i){
 String[] medStrings = { "Continue routine observation", "Involve the nurse-in-charge immediately", "Call the registrar for immediate review" };
//Create the combo box, select item at index 4.
//Indices start at 0, so 4 specifies the pig.
JComboBox medList = new JComboBox(medStrings);
    int get = Charlie_slection.getSelectedIndex();
if(get+i==0|| get+i==1)
    Charlie_output.setText(medStrings[0]);
else if(get+i==2 || get+i==3)
    Charlie_output.setText(medStrings[0]);
else if(get+i==4 || get+i==5)
    Charlie_output.setText(medStrings[1]);
else if(get+i>=6)
    Charlie_output.setText(medStrings[2]);   
     
 }    
 
 public void Grace_judge(int i){
 String[] medStrings = { "Continue routine observation", "Involve the nurse-in-charge immediately", "Call the registrar for immediate review" };
//Create the combo box, select item at index 4.
//Indices start at 0, so 4 specifies the pig.
JComboBox medList = new JComboBox(medStrings);
    int get = Grace_selection.getSelectedIndex();
if(get+i==0|| get+i==1)
    Grace_output.setText(medStrings[0]);
else if(get+i==2 || get+i==3)
    Grace_output.setText(medStrings[0]);
else if(get+i==4 || get+i==5)
    Grace_output.setText(medStrings[1]);
else if(get+i>=6)
    Grace_output.setText(medStrings[2]);     
 }  

  public void Ian_judge(int i){
 String[] medStrings = { "Continue routine observation", "Involve the nurse-in-charge immediately", "Call the registrar for immediate review" };
//Create the combo box, select item at index 4.
//Indices start at 0, so 4 specifies the pig.
JComboBox medList = new JComboBox(medStrings);
    int get = Ian_selection.getSelectedIndex();
if(get+i==0|| get+i==1)
    Ian_output.setText(medStrings[0]);
else if(get+i==2 || get+i==3)
    Ian_output.setText(medStrings[0]);
else if(get+i==4 || get+i==5)
    Ian_output.setText(medStrings[1]);
else if(get+i>=6)
    Ian_output.setText(medStrings[2]);     
 } 
 
   public void Kelly_judge(int i){
 String[] medStrings = { "Continue routine observation", "Involve the nurse-in-charge immediately", "Call the registrar for immediate review" };
//Create the combo box, select item at index 4.
//Indices start at 0, so 4 specifies the pig.
JComboBox medList = new JComboBox(medStrings);
    int get = Kelly_selection.getSelectedIndex();
if(get+i==0|| get+i==1)
    Kelly_output.setText(medStrings[0]);
else if(get+i==2 || get+i==3)
    Kelly_output.setText(medStrings[0]);
else if(get+i==4 || get+i==5)
    Kelly_output.setText(medStrings[1]);
else if(get+i>=6)
    Kelly_output.setText(medStrings[2]);     
 } 
   
   public void Elise_judge(int i){
 String[] medStrings = { "Continue routine observation", "Involve the nurse-in-charge immediately", "Call the registrar for immediate review" };
//Create the combo box, select item at index 4.
//Indices start at 0, so 4 specifies the pig.
JComboBox medList = new JComboBox(medStrings);
    int get = Elise_selection.getSelectedIndex();
if(get+i==0|| get+i==1)
    Elise_output.setText(medStrings[0]);
else if(get+i==2 || get+i==3)
    Elise_output.setText(medStrings[0]);
else if(get+i==4 || get+i==5)
    Elise_output.setText(medStrings[1]);
else if(get+i>=6)
    Elise_output.setText(medStrings[2]);     
 }    
   
public int count_random(int k){
        Random random = new Random();
        return random.nextInt(7);
}    
    
    
    
 public void breath_table(int br){
       int value=0;
       if (br<=8)
           value=3;
       else if (br>=9 && br<=20)
           value=0;
       else if (br>=21 && br<=30)
           value=1;
       else if (br>=31 && br<=35)
           value=2;
       else
           value=3;
       count_res=count_res+value;
   }
   
      public void oxygen_table(int spo2){
       int value=0;
       if (spo2<85)
           value=3;
       else if (spo2>=85 && spo2<=89)
           value=2;
       else if (spo2>=90 && spo2<=92)
           value=1;
       else
           value=0;
       count_oxy=count_oxy+value;
   }
      
      
      
      
      public void temperature_table(float temp){
       int value=0;
       if (temp<34.0)
           value=3;
       else if (temp>=34.0 && temp<=34.9)
           value=2;
       else if (temp>=35.0 && temp<=35.9)
           value=1;
       else if (temp>=36.0 && temp<=37.9)
           value=0;
       else if (temp>=38.0 && temp<=38.4)
           value=1;
       else 
           value=2;
       count_tem=count_tem+value;
   }    
      
       public void blood_table(int systolic){
       int value=0;
       if (systolic<=69)
           value=3;
       else if (systolic>=70 && systolic<=79)
           value=2;
       else if (systolic>=80 && systolic<=99)
           value=1;
       else if (systolic>=100 && systolic<=199)
           value=0;
       else
           value=2;
        count_blo=count_blo+value;
   }
      
       public void heartrate_table(int hr){
       int value=0;
       if (hr<=29)
           value=3;
       else if (hr>=30 && hr<=39)
           value=2;
       else if (hr>=40 && hr<=49)
           value=1;
       else if (hr>=50 && hr<=99)
           value=0;
       else if (hr>=100 && hr<=109)
           value=1;
       else if (hr>=110 && hr<=129)
           value=2;
       else
           value=3;
       count_hr=count_hr+value;
   }
       
      public void traffic_light(int count){
          
       if (count==0 || count==1){
           
           //Charlie.setBackground(Color.GREEN);
       }
       else if (count==2 || count==3){
           
           //Charlie.setBackground(Color.YELLOW);
       }
       else if (count==4 || count>=5){
           
           //Charlie.setBackground(Color.RED); 
           //Alice.setBackground(Color.GREEN); 
       }   
         
      }
      
      
      
      
      
   /*public int generateReading(int kind) {
        Random random = new Random();
        int returnReading = 0;
        switch (kind) {
            case 0: //Breathing Rate
                returnReading = random.nextInt(35) + 5 + 1;
                break;
            case 1: //SPO2
                returnReading = random.nextInt(20) + 80 + 1;
                break;
            case 2: //Blood Pressure - SYSTOLIC
                returnReading = random.nextInt(150) + 60 + 1;
                break;
            case 3: //Heart Rate
                returnReading = random.nextInt(20) + 140 + 1;
                break;
            default:
                break;
        }
        
        return returnReading;
    }
    
 public float generateReading(float kind) {
        Random random = new Random();
        return random.nextFloat() * 6 + 33;
    }   */
    
    
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel9 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButton5 = new javax.swing.JButton();
        jButton6 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        Time = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        Alice_selection = new javax.swing.JComboBox();
        Charlie_slection = new javax.swing.JComboBox();
        Grace_selection = new javax.swing.JComboBox();
        Ian_selection = new javax.swing.JComboBox();
        Kelly_selection = new javax.swing.JComboBox();
        Elise_selection = new javax.swing.JComboBox();
        Alice_output = new javax.swing.JLabel();
        Charlie_output = new javax.swing.JLabel();
        Grace_output = new javax.swing.JLabel();
        Ian_output = new javax.swing.JLabel();
        Kelly_output = new javax.swing.JLabel();
        Alice = new javax.swing.JLabel();
        Charlie = new javax.swing.JLabel();
        Grace = new javax.swing.JLabel();
        Ian = new javax.swing.JLabel();
        Kelly = new javax.swing.JLabel();
        Elise = new javax.swing.JLabel();
        Elise_output = new javax.swing.JLabel();
        jButton8 = new javax.swing.JButton();
        jButton9 = new javax.swing.JButton();
        jButton10 = new javax.swing.JButton();
        jButton11 = new javax.swing.JButton();
        jButton12 = new javax.swing.JButton();
        jButton13 = new javax.swing.JButton();

        jLabel9.setText("jLabel9");

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 100, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setAlwaysOnTop(true);
        setAutoRequestFocus(false);
        setBackground(java.awt.Color.black);
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));

        jButton2.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/MAN1.png")); // NOI18N
        jButton2.setText("Charlie Dean");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/WOMAN.png")); // NOI18N
        jButton3.setText("Elise Foster");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/MAN1.png")); // NOI18N
        jButton4.setText("Ian Jones");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButton5.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/WOMAN.png")); // NOI18N
        jButton5.setText("Kelly Lawrence");
        jButton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton5ActionPerformed(evt);
            }
        });

        jButton6.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/WOMAN.png")); // NOI18N
        jButton6.setText("Grace Hughes");
        jButton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton6ActionPerformed(evt);
            }
        });

        jButton1.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/WOMAN.png")); // NOI18N
        jButton1.setText("Alice Bailey");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        Time.setFont(new java.awt.Font("Cantarell", 2, 14)); // NOI18N
        Time.setText("Time");

        Alice_selection.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Alert(0)", "Verbal (1)", "Pain (2)", "Unresponsive (3)" }));
        Alice_selection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Alice_selectionActionPerformed(evt);
            }
        });

        Charlie_slection.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Alert(0)", "Verbal (1)", "Pain (2)", "Unresponsive (3)" }));
        Charlie_slection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Charlie_slectionActionPerformed(evt);
            }
        });

        Grace_selection.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Alert(0)", "Verbal (1)", "Pain (2)", "Unresponsive (3)" }));
        Grace_selection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Grace_selectionActionPerformed(evt);
            }
        });

        Ian_selection.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Alert(0)", "Verbal (1)", "Pain (2)", "Unresponsive (3)" }));
        Ian_selection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Ian_selectionActionPerformed(evt);
            }
        });

        Kelly_selection.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Alert(0)", "Verbal (1)", "Pain (2)", "Unresponsive (3)" }));
        Kelly_selection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Kelly_selectionActionPerformed(evt);
            }
        });

        Elise_selection.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Alert(0)", "Verbal (1)", "Pain (2)", "Unresponsive (3)" }));
        Elise_selection.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Elise_selectionActionPerformed(evt);
            }
        });

        Alice_output.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Alice_output.setText("jLabel1");

        Charlie_output.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Charlie_output.setText("jLabel2");

        Grace_output.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Grace_output.setText("jLabel3");

        Ian_output.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Ian_output.setText("jLabel4");

        Kelly_output.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Kelly_output.setText("jLabel5");

        Alice.setBackground(java.awt.Color.lightGray);
        Alice.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Alice.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Alice.setText("jLabel6");
        Alice.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        Alice.setOpaque(true);

        Charlie.setBackground(java.awt.Color.lightGray);
        Charlie.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Charlie.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Charlie.setText("jLabel7");
        Charlie.setOpaque(true);

        Grace.setBackground(java.awt.Color.lightGray);
        Grace.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Grace.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Grace.setText("jLabel8");
        Grace.setOpaque(true);

        Ian.setBackground(java.awt.Color.lightGray);
        Ian.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Ian.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Ian.setText("jLabel10");
        Ian.setOpaque(true);

        Kelly.setBackground(java.awt.Color.lightGray);
        Kelly.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Kelly.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Kelly.setText("jLabel11");
        Kelly.setOpaque(true);

        Elise.setBackground(java.awt.Color.lightGray);
        Elise.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Elise.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        Elise.setText("jLabel12");
        Elise.setOpaque(true);

        Elise_output.setFont(new java.awt.Font("Cantarell", 0, 24)); // NOI18N
        Elise_output.setText("jLabel1");

        jButton8.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/LINE.png")); // NOI18N
        jButton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton8ActionPerformed(evt);
            }
        });

        jButton9.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/LINE.png")); // NOI18N
        jButton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton9ActionPerformed(evt);
            }
        });

        jButton10.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/LINE.png")); // NOI18N
        jButton10.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton10ActionPerformed(evt);
            }
        });

        jButton11.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/LINE.png")); // NOI18N
        jButton11.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton11ActionPerformed(evt);
            }
        });

        jButton12.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/LINE.png")); // NOI18N
        jButton12.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton12ActionPerformed(evt);
            }
        });

        jButton13.setIcon(new javax.swing.ImageIcon("/afs/inf.ed.ac.uk/user/s15/s1513746/Desktop/hci/LINE.png")); // NOI18N
        jButton13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton13ActionPerformed(evt);
            }
        });

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .addContainerGap()
                        .add(jSeparator1))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createSequentialGroup()
                                .add(178, 178, 178)
                                .add(Time, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 283, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(layout.createSequentialGroup()
                                .add(58, 58, 58)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                        .add(org.jdesktop.layout.GroupLayout.TRAILING, Charlie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 118, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(org.jdesktop.layout.GroupLayout.TRAILING, Grace, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 118, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(org.jdesktop.layout.GroupLayout.TRAILING, Ian, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 118, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(org.jdesktop.layout.GroupLayout.TRAILING, Alice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 118, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                    .add(Kelly, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 128, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                    .add(Elise, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 120, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                .add(28, 28, 28)
                                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                    .add(layout.createSequentialGroup()
                                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jButton2)
                                            .add(Alice_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(Charlie_slection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(jButton6)
                                            .add(Grace_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(jButton1)
                                            .add(jButton4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 154, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(Ian_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(jButton5)
                                            .add(Kelly_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                            .add(jButton3))
                                        .add(10, 10, 10)
                                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                .add(layout.createSequentialGroup()
                                                    .add(jButton9)
                                                    .add(34, 34, 34)
                                                    .add(Charlie_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 355, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                                .add(layout.createSequentialGroup()
                                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                        .add(jButton11)
                                                        .add(jButton13)
                                                        .add(layout.createSequentialGroup()
                                                            .add(1, 1, 1)
                                                            .add(jButton10))
                                                        .add(layout.createSequentialGroup()
                                                            .add(2, 2, 2)
                                                            .add(jButton12)))
                                                    .add(23, 23, 23)
                                                    .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                                        .add(Ian_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 376, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                        .add(Grace_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 376, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                        .add(Kelly_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 344, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                                        .add(Elise_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 323, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                                            .add(layout.createSequentialGroup()
                                                .add(jButton8)
                                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .add(Alice_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 365, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                                    .add(Elise_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                        .add(0, 90, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .add(27, 27, 27)
                .add(Time)
                .add(18, 18, 18)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .add(18, 18, 18)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(Alice, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 108, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jButton1)
                                .add(Alice_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jButton8))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(Alice_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 217, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(layout.createSequentialGroup()
                        .add(Charlie, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 111, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(65, 65, 65)
                        .add(Grace, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 109, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton2)
                            .add(Charlie_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jButton9))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(Charlie_slection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(66, 66, 66)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jButton6)
                                .add(Grace_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 36, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(jButton10))
                        .add(18, 18, 18)
                        .add(Grace_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(26, 26, 26)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton4)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(Ian_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 28, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jButton11)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(Ian_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(77, 77, 77)
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton5)
                            .add(Kelly_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 38, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                            .add(jButton12))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 184, Short.MAX_VALUE)
                        .add(Kelly_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(layout.createSequentialGroup()
                        .add(Ian, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 115, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(66, 66, 66)
                        .add(Kelly, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 118, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .add(105, 105, 105)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(layout.createSequentialGroup()
                        .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jButton3)
                            .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(Elise_output, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 29, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(jButton13)))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .add(Elise_selection, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(Elise, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 113, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .add(24, 24, 24))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        //user s= new user();
        //s.setVisible(true);
        dispose();
     user s= new user();
     s.setVisible(true);
     
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
                dispose();
     user2 s= new user2();
     s.setVisible(true);
    }//GEN-LAST:event_jButton2ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        // TODO add your handling code here:
                dispose();
     user3 s= new user3();
     s.setVisible(true);
    }//GEN-LAST:event_jButton3ActionPerformed

    private void jButton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton6ActionPerformed
        // TODO add your handling code here:
                dispose();
     user6 s= new user6();
     s.setVisible(true);
    }//GEN-LAST:event_jButton6ActionPerformed

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        // TODO add your handling code here:
                dispose();
     user4 s= new user4();
     s.setVisible(true);
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton5ActionPerformed
        // TODO add your handling code here:
                dispose();
     user5 s= new user5();
     s.setVisible(true);
    }//GEN-LAST:event_jButton5ActionPerformed

    private void Alice_selectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Alice_selectionActionPerformed
        // TODO add your handling code here:
        Alice_judge(p1);
        
    }//GEN-LAST:event_Alice_selectionActionPerformed

    private void Charlie_slectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Charlie_slectionActionPerformed
        // TODO add your handling code here:
        Charlie_judge(p2);
    }//GEN-LAST:event_Charlie_slectionActionPerformed

    private void Grace_selectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Grace_selectionActionPerformed
        // TODO add your handling code here:
        Grace_judge(p3);
    }//GEN-LAST:event_Grace_selectionActionPerformed

    private void Ian_selectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Ian_selectionActionPerformed
        // TODO add your handling code here:
        Ian_judge(p4);
    }//GEN-LAST:event_Ian_selectionActionPerformed

    private void Kelly_selectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Kelly_selectionActionPerformed
        // TODO add your handling code here:
        Kelly_judge(p6);
    }//GEN-LAST:event_Kelly_selectionActionPerformed

    private void Elise_selectionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Elise_selectionActionPerformed
        // TODO add your handling code here:
        Elise_judge(p5);
    }//GEN-LAST:event_Elise_selectionActionPerformed

    private void jButton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton8ActionPerformed
        // TODO add your handling code here:
     dispose();
     HistoryJFrame s= new HistoryJFrame();
     s.setVisible(true);
    }//GEN-LAST:event_jButton8ActionPerformed

    private void jButton12ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton12ActionPerformed
        // TODO add your handling code here:
          dispose();
     HistoryJFrame s= new HistoryJFrame();
     s.setVisible(true);
    }//GEN-LAST:event_jButton12ActionPerformed

    private void jButton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton9ActionPerformed
        // TODO add your handling code here:
          dispose();
     HistoryJFrame s= new HistoryJFrame();
     s.setVisible(true);
    }//GEN-LAST:event_jButton9ActionPerformed

    private void jButton10ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton10ActionPerformed
        // TODO add your handling code here:
          dispose();
     HistoryJFrame s= new HistoryJFrame();
     s.setVisible(true);
    }//GEN-LAST:event_jButton10ActionPerformed

    private void jButton11ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton11ActionPerformed
        // TODO add your handling code here:
          dispose();
     HistoryJFrame s= new HistoryJFrame();
     s.setVisible(true);
    }//GEN-LAST:event_jButton11ActionPerformed

    private void jButton13ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton13ActionPerformed
        // TODO add your handling code here:
          dispose();
     HistoryJFrame s= new HistoryJFrame();
     s.setVisible(true);
    }//GEN-LAST:event_jButton13ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;


                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PatientJFrame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PatientJFrame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PatientJFrame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PatientJFrame.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    new PatientJFrame().setVisible(true);
                } catch (IOException ex) {
                    Logger.getLogger(PatientJFrame.class.getName()).log(Level.SEVERE, null, ex);
                }

            }
        });
    }

    /**
     * Create a random int number
     */
    public int generateReading(int kind) {
        Random random = new Random();
        int returnReading = 0;
        switch (kind) {
            case 0: //Breathing Rate
                returnReading = random.nextInt(35) + 1;
                
                break;
            case 1: //SPO2
                returnReading = random.nextInt(92) + 1;
                break;
            case 2: //Blood Pressure - SYSTOLIC
                returnReading = random.nextInt(200) +1;
                break;
            case 3: //Heart Rate
                returnReading = random.nextInt(130)+ 1;
                break;
            default:
                break;
        }
        
        return returnReading;
    }

    /**
     * Create a random float number - body temp
     */
    public float generateReading(float kind) {
        Random random = new Random();
        return random.nextFloat() * 6 + 33;
    }
    
    /**
     * Create a random int number in a range - you might want to use this for testing
     */
    public int generateReadingInRange(int lower, int upper) {
        Random random = new Random();
        return random.nextInt(upper - lower) + lower + 1;
    }
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Alice;
    private javax.swing.JLabel Alice_output;
    private javax.swing.JComboBox Alice_selection;
    private javax.swing.JLabel Charlie;
    private javax.swing.JLabel Charlie_output;
    private javax.swing.JComboBox Charlie_slection;
    private javax.swing.JLabel Elise;
    private javax.swing.JLabel Elise_output;
    private javax.swing.JComboBox Elise_selection;
    private javax.swing.JLabel Grace;
    private javax.swing.JLabel Grace_output;
    private javax.swing.JComboBox Grace_selection;
    private javax.swing.JLabel Ian;
    private javax.swing.JLabel Ian_output;
    private javax.swing.JComboBox Ian_selection;
    private javax.swing.JLabel Kelly;
    private javax.swing.JLabel Kelly_output;
    private javax.swing.JComboBox Kelly_selection;
    private javax.swing.JLabel Time;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton10;
    private javax.swing.JButton jButton11;
    private javax.swing.JButton jButton12;
    private javax.swing.JButton jButton13;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton5;
    private javax.swing.JButton jButton6;
    private javax.swing.JButton jButton8;
    private javax.swing.JButton jButton9;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables
}
