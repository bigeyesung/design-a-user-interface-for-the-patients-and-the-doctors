package patientview;
import java.awt.*;
import java.awt.Toolkit;
import java.awt.event.*;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 *
 * @author s1513746
 */
public class ButtonRollover {
    private String normalImagePath;
    private String rolloverImagePath;

    public ButtonRollover(String normalImagePath, String rolloverImagePath) {
        this.normalImagePath = normalImagePath;
        this.rolloverImagePath = rolloverImagePath;
    }

    public void apply(AbstractButton abstractButton) {
        abstractButton.setBorderPainted(false);
        abstractButton.setBackground(new Color(0, 0, 0, 0));
        abstractButton.setRolloverIcon(createImageIcon(rolloverImagePath));
        abstractButton.setIcon(createImageIcon(normalImagePath));
    }

    private ImageIcon createImageIcon(String path) {
        java.net.URL imgURL = getClass().getResource(path);
        if (imgURL != null) {
            return new ImageIcon(imgURL);
        } else {
            System.err.println("Couldn't find file: " + path);
            return null;
        }
    }
}
